import os
import json
import requests

pixabay_api_begin = "https://pixabay.com/api/?key="
pixabay_api_end = "&image_type=photo"

name_folder = 'images'

def main():
    global name_folder, api_key
    content = input('What images do you want? :').split()
    num_images = int(input('How many images do you want? :'))
    api_key = input("What's your api key on pixabay? :")

    # make direcotry for images will be downloaded
    name_folder = "_".join(content)
    if not os.path.exists(name_folder):
    	os.mkdir(name_folder)
	
    print('Searching...')
    download_imgs(content, num_images)


def download_imgs(search, num_images):
    # create url string for request
    search_query = "+".join(search)
    url = pixabay_api_begin + api_key + '&q=' + search_query + pixabay_api_end
    print("URL :", url)
    
    # number of images per page
    div200 = num_images // 200
    num_pics_last_request = num_images % 200
    if div200 == 0:
        num_requests = 1
    else:
        num_requests = div200 + 1

    # request json format data and download images using url
    for request in range(num_requests):
        page = request + 1
        if request == num_requests - 1:
            per_page = num_pics_last_request
        else:
            per_page = 200
    
        # parameters for the api request
        parameters = {
            'page' : page,
            'per_page' : per_page
        }

        # request url api returns json format
        web_response = requests.get(url, params = parameters) 
        print("response_status:",web_response.status_code)
        
        # get json data as a python dictionary
        dict = web_response.json()
        print("total number of images:", dict['total'])
        print("total hits:", dict['totalHits'])
        
        # when the number of total images is less than what user wants
        if dict['total'] <  num_images:
            print("You can download up to only {} images".format(dict['total']))

        # download images
        for i in range(per_page):
            image_url = dict['hits'][i]['webformatURL']
            img_response = requests.get(image_url)
            image = img_response.content
        
            filename = name_folder + '/' + "_".join(search) + str(request * 200 + i+1) + ".jpg"
            with open(filename, 'wb') as f:
                f.write(image)

    print("Done")
    
if __name__ == "__main__":	
    main()

	

	
	
	
	
